import slsk from 'slsk-client'
import fs from 'fs'

slsk.connect({ user: "setup-python", pass: "qazsxecrv" }, (err, client) => {
    if (err) return console.log(err)
    client.search({ req: process.argv.slice(2).join(' ') }, (err, res) => {
        if (err) return console.log(err)
        let alwaysMoreBitrate = res.filter(song => song.file.includes('.mp3') && song.slots == true)
        let data = JSON.stringify(alwaysMoreBitrate, null, 2)
        fs.writeFileSync(`soulseek.json`, data)
        process.exit()
    })
})

