#! /usr/bin/env python3
import subprocess
import os
import json
import soundcloud
import re
import requests
from pprint import pprint

reg1 = "https://soundcloud.com"
CLIENT_ID="80baf2537bedfc558f35242c76cfc8b2"

while 42:
    url = input("url soundcloud : ")
    if url == "exit":
        exit()
    if reg1 in url:
        print("url is good")
        client = soundcloud.Client(client_id=CLIENT_ID)
        track = client.get('/resolve', url=url)
        if "sets" in url:
            print("its a playlist")
            # pprint(track.tracks)
            # boucle dans la playlist => action a effectuer pour chaque track
            for t in track.tracks:
                user = t['user']['username']
                title = t['title']
                duration = t['duration']
                dl = t['downloadable']
                # sanitize title
                title_w_parentheses = ""
                for c in title:
                    if c == "(":
                        break
                    if c == "&":
                        title_w_parentheses += " and "
                    elif c == "'":
                        title_w_parentheses += " "
                    else:
                        title_w_parentheses += c
                arr_title = title_w_parentheses.split('-')
                definitif_title = []
                if len(arr_title) != 2:
                    definitif_title.append(user)
                    definitif_title.append(arr_title[0])
                else:
                    definitif_title.append(arr_title[0])
                    definitif_title.append(arr_title[1])
                # print les variables interessantes
                pprint(definitif_title)
                pprint(duration)
                pprint(dl)
                # if dl == True:
                #     dl_url = t['download_url']
                #     contents = requests.get(dl_url + "?client_id=" + CLIENT_ID, allow_redirects=True)
                #     open(definitif_title[0] + " - " + definitif_title[1], 'wb').write(contents.content)
                # search with full title 
                subprocess.call("node " + os.getcwd() + f"/dist/search.js {definitif_title[0]} {definitif_title[1]}"  , shell=True)
                with open("soulseek.json", "r") as read_file:
                    data = json.load(read_file)
                # if no result search with only name of the track and search for the autor
                if data == []:
                    pprint("==> first atempt failed trying with the name of the song...")
                    subprocess.call("node " + os.getcwd() + f"/dist/search.js {definitif_title[1]}"  , shell=True)
                    with open("soulseek.json", "r") as read_file:
                        data = json.load(read_file)
                    music_found = []
                    for obj in data:
                        name_file = obj['file']
                        name_user = definitif_title[0].lower()
                        i = 0
                        for c in name_file:
                            if i >= len(name_user):
                                break
                            if c == name_user[i]:
                                i += 1
                        if i == len(name_user):
                            music_found.append(obj)
                    if music_found == []:
                        pprint("sorry we cannot find the music")
                    else:
                        pprint(music_found)
                        with open("music.json", "w") as write_file:
                            json.dump(music_found, write_file, indent=4)
                        subprocess.call("node " + os.getcwd() + f"/dist/download.js {definitif_title[0]} {definitif_title[1]}"  , shell=True)
                else:
                    pprint(data)
                    with open("music.json", "w") as write_file:
                        json.dump(data, write_file, indent=4)
                    subprocess.call("node " + os.getcwd() + f"/dist/download.js {definitif_title[0]} {definitif_title[1]}"  , shell=True)

                # if no result search with autor and search for the name of the track
                ######
                # if downloadable is true dl la track
                # downloadable false go soulseek

        else:
            print("its a track")
            pprint(track.title)
            pprint(track.duration)
            pprint(track.downloadable)
            ######
            # if downloadable is true dl la track
            # downloadable false go soulseek
    else:
        print("url is not soundcloud's url")
    
    # subprocess.call("node " + os.getcwd() + "/dist/server.js " + stdin, shell=True)
    # with open('soulseek.json', 'r') as read_file:
        # data = json.load(read_file)
    # print(json.dumps(data, indent=2))